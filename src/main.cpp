/*
 * Sample program for DocodeModem to draw string on the OLED display of external I2C  
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include <docodemo.h>
#include <SSD1306.h>

DOCODEMO Dm;
SSD1306 display(0x3c, EXTERNAL_SDA, EXTERNAL_SCL, GEOMETRY_128_64, I2C_TWO, 400000);

void setup() {
  Dm.begin();

  display.setI2cAutoInit(true);
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);
  display.setI2cAutoInit(false);
}

int count = 0;
void loop() {

  portTickType xLastExecutionTime = xTaskGetTickCount();
  while(1)
  {
    display.clear();
    display.drawString(0, 0, "Hello Wolrd");
    display.drawString(0, 15, "count : " + String(count++));
    display.drawString(0, 30, String(Dm.readExADC()) + " V");//AD値は補正が必要です。
    display.drawString(0, 45, "Circuit Design,Inc.");
    display.display();

    vTaskDelayUntil(&xLastExecutionTime, 1 * 1000 / portTICK_PERIOD_MS);
  }
}